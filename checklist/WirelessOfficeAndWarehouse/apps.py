from django.apps import AppConfig


class WirelessofficeandwarehouseConfig(AppConfig):
    name = 'WirelessOfficeAndWarehouse'
