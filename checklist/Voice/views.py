from django.shortcuts import render
import json
from django.http import HttpResponse
from django.views import View
from django.shortcuts import get_object_or_404
from checklist.Voice.models import voicedata
class voicedatalist(View):
    def get(self, request):
        response = list()
        for insp in voicedata.object.all:
            response.append({
                'id': insp.id,
                'inspector': insp.inspector,
                'workname': insp.workname,
                'descriptions': insp.descriptions,
                'ip': insp.ip,
                'howto': insp.howto
            })
        return HttpResponse(json.dumps(response), content_type='application/json')
class voicedataDetail(View):
    def get(self, request, id):
        det = get_object_or_404(voicedata, id=id)
        response = {
            'id': det.id,
            'inspector': det.inspector,
            'workname': det.workname,
            'descriptions': det.descriptions,
            'ip': det.ip,
            'howto': det.howto
        }
        return HttpResponse(json.dumps(response), content_type='application/json')

