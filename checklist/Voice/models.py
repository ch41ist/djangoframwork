from django.db import models

class voicedata(models.Model):
    inspector = models.CharField(max_length=200)
    workname = models.CharField(max_length=200)
    descriptions = models.CharField(max_length=1000, default="")
    ip = models.CharField(max_length=1000, default="")
    howto = models.CharField(max_length=1000,default="")
