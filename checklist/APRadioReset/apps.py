from django.apps import AppConfig


class ApradioresetConfig(AppConfig):
    name = 'APRadioReset'
