from django.apps import AppConfig


class F5LoadbalanceConfig(AppConfig):
    name = 'F5LoadBalance'
